import datetime


def con(month):
    if month == 'jan':
        month = '01'
    elif month == 'Feb':
        month = '02'
    elif month == 'Mar':
        month = '03'
    elif month == 'Apr':
        month = '04'
    elif month == 'May':
        month = '05'
    elif month == 'Jun':
        month = '06'
    elif month == 'Jul':
        month = '07'
    elif month == 'Aug':
        month = '08'
    elif month == 'Sep':
        month = '09'
    elif month == 'Oct':
        month = '10'
    elif month == 'Nov':
        month = '11'
    elif month == 'Dec':
        month = '12'
    return int(month)


def Date_c(date):
    date = str(date)
    n = date.split("-")
    date_f = datetime.datetime(
        int(n[2]), con(n[1]), int(n[0])
    ).date()
    return date_f


""" print(Date('09-Nov-2020')) """
