import csv



def work_in_csv(path):
    with open(path) as File:
        content = csv.DictReader(File)
        count = 0
        movements = {}
        trans_per_year = {}

        for line in content:
            count += 1
            des = line.get('Description')

            if des in movements.keys():
                movements[des] += 1
            else:
                movements[des] = 1

            year = line.get('Date')[-4:]
            if year in trans_per_year.keys():
                trans_per_year[year] += 1
            else:
                trans_per_year[year] = 1

        return {'rows_in_file': count, 'movements': movements, 'transaction_per_year': trans_per_year}